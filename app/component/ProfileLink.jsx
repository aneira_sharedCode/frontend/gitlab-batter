// Dependencies
import React, {Component} from 'react';

// Assets


class ProfileLink extends Component {
    render() {
        return (
            <a href={'https://www.fb.com/' + this.props.username}>
                {this.props.username}
            </a>
        );
    }
}

export default ProfileLink;